FROM alpine:latest
RUN apk add --no-cache git

RUN mkdir /var/data
VOLUME /var/data
WORKDIR /var/data